# My trial on learning Rust

This repo is an implementation on linked lists, following a 
[guide](https://rust-unofficial.github.io/too-many-lists/)

The repo with the source code is on [github](https://github.com/rust-unofficial/too-many-lists)

## My journey

I'm trying to keep up with the book and get use to how Rust works.

I'm more use to C, so implementing a simple linked list in Rust is a challenge.

So far am getting used to ownership, and references, but I still need to play
around more with some instructions and cases like as_ref(), map, and other methodes.

So far playing with memory in heap is not that straight forward as it is in C
and it's harder to keep track of pointers and references, at least it is for me
