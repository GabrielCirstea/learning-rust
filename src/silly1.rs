pub struct Stack<T>{
    head: Link<T>,
}

type Link<T> = Option<Box<Node<T>>>;

struct Node<T> {
    elem: T,
    next: Link<T>,
}

impl<T> Stack<T> {
    pub fn new() -> Self {
        Stack { head: None }      // no ';' means auto return
    }

    pub fn push(&mut self, elem: T) {
        let new_node = Box::new(Node {
            elem: elem,
            next: None,
        });
        self.push_node(new_node);
    }

    fn push_node(&mut self, mut node: Box<Node<T>>) {
        node.next = self.head.take();
        self.head = Some(node);
    }

    pub fn pop(&mut self) -> Option<T> {
        self.pop_node().map(|node| {
            return node.elem;
        })
    }

    fn pop_node(&mut self) -> Option<Box<Node<T>>> {
        return self.head.take().map(|mut node| {
            self.head = node.next.take();
            return node;
        });
    }

    pub fn peek(&self) -> Option<&T> {
        self.head.as_ref().map(|node| {
            &node.elem
        })
    }
     pub fn peek_mut(&mut self) -> Option<&mut T> {
         self.head.as_mut().map(|node| {
             &mut node.elem
         })
     }
}

impl<T> Drop for Stack<T>{
    fn drop(&mut self){
        let mut cur_link = self.head.take();
        while let Some(mut boxed_node) = cur_link {
            cur_link = boxed_node.next.take();
        }
    }
}

pub struct List<T> {
    left: Stack<T>,
    right: Stack<T>,
}

impl<T> List<T> {
    fn new() -> Self {
        return List{left: Stack::new(), right: Stack::new()};
    }

    pub fn push_left(&mut self, elem: T) {
        self.left.push(elem);
    }
    pub fn push_right(&mut self, elem: T) {
        self.right.push(elem);
    }
    pub fn pop_left(&mut self) -> Option<T> {
        return self.left.pop();
    }
    pub fn pop_right(&mut self) -> Option<T> {
        return self.right.pop();
    }
    pub fn peek_left(&mut self) -> Option<&T> {
        return self.left.peek();
    }
    pub fn peek_right(&mut self) -> Option<&T> {
        return self.right.peek();
    }
    pub fn peek_left_mut(&mut self) -> Option<&mut T> {
        return self.left.peek_mut();
    }
    pub fn peek_right_mut(&mut self) -> Option<&mut T> {
        return self.right.peek_mut();
    }

    pub fn go_left(&mut self) -> bool {
        return self.left.pop_node().map(|node| {
            self.right.push_node(node);
        }).is_some();
    }

    pub fn go_right(&mut self) -> bool {
        return self.right.pop_node().map(|node| {
            self.left.push_node(node);
        }).is_some();
    }
}

#[cfg(test)]
mod test{
    use super::List;

    #[test]
    fn walk_aboot(){
        let mut list = List::new();

        list.push_left(0);
        list.push_right(1);
        assert_eq!(list.peek_left(), Some(&0));
        assert_eq!(list.peek_right(), Some(&1));

        list.push_left(2);
        list.push_left(3);
        list.push_right(4);

        while list.go_left() {}

        assert_eq!(list.pop_left(), None);
        assert_eq!(list.pop_right(), Some(0));
        assert_eq!(list.pop_right(), Some(2));

        list.push_left(5);
        assert_eq!(list.pop_left(), Some(5));
        assert_eq!(list.pop_right(), Some(3));
        assert_eq!(list.pop_right(), Some(4));
        assert_eq!(list.pop_right(), Some(1));

        assert_eq!(list.pop_right(), None);
        assert_eq!(list.pop_left(), None);
    }
}
