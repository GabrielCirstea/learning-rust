use std::mem;

pub struct List{
    head: Link,
}

struct Node {
    elem: i32,
    next: Link,
}

enum Link{
    Empty,
    More(Box<Node>),
}

impl List {
    pub fn new() -> Self {
        List { head: Link::Empty }      // no ';' means auto return
    }

    pub fn push(&mut self, elem: i32) {
        let new_node = Box::new(Node {
            elem: elem,
            next: mem::replace(&mut self.head, Link::Empty),
        });
        self.head = Link::More(new_node);
    }

    pub fn pop(&mut self) -> Option<i32> {
        let result;
        match mem::replace(&mut self.head, Link::Empty) {
            Link::Empty => {
                result = None;
            }
            Link::More(node) => {
                result = Some(node.elem);
                self.head = node.next;
            }
        };
        return result;
    }
}

impl Drop for List{
    fn drop(&mut self){
        let mut cur_link = mem::replace(&mut self.head, Link::Empty);
        while let Link::More(mut boxed_node) = cur_link {
            cur_link = mem::replace(&mut boxed_node.next, Link::Empty);
        }
    }
}

#[cfg(test)]
mod test{
    use super::List;
    #[test]
    fn basic(){
        let mut list = List::new();

        // Should be empty
        assert_eq!(list.pop(), None);

        // put stuff in list
        list.push(1);
        list.push(2);
        list.push(3);
        list.push(4);

        // pop test
        assert_eq!(list.pop(),Some(4));
        assert_eq!(list.pop(),Some(3));

        list.push(4);
        list.push(5);

        assert_eq!(list.pop(),Some(5));
        assert_eq!(list.pop(),Some(4));
        // empty list
        assert_eq!(list.pop(),Some(2));
        assert_eq!(list.pop(),Some(1));
        assert_eq!(list.pop(),None);
    }
}
